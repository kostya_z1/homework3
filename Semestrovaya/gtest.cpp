#include <gtest/gtest.h>

#include "Roll.h"
TEST(Lrolltest, lineRoll) // ���� ��������� ������
{
	List *begin = NULL; // ��������������� ���������
	int n = Lroll(begin); // ...
	ASSERT_EQ(bool(begin), 0); // ��������� ������������ ������� ��� ������� ������
	ASSERT_EQ(n, 0); // ��������� ������������ �������� �������
	for (int k = 1; k<=10; k++) 
	{
		begin = f(k); // ������� �������� ������ ����� k
		n = Lroll(begin); // ��������� ������������ ��������
		for (int i=k; i>=1; i--) // �������� �� ������
		{
			ASSERT_EQ(begin->key, i); // �������, ���� �� �������� � �������� �������
			begin = begin->next;
		}
		ASSERT_EQ(bool(begin), 0); // �������, ���������� �� ������
		ASSERT_EQ(n, 1); // ��������� ������������ ������������� ��������
	}
}
TEST(Lrolltest, cycleRoll) // ���� ������������ ������
{
	List* begin;
	for (int k=1; k<=10; k++) // ������ ������
	{
		for (int j=1; j<=k; j++) // ���� ��������� end->next;
		{
			begin = f(k, j); // ������� ������ ����� k, � ������� ��������� ������� ��������� �� j-��
			int n = Lroll(begin); // ���������� ������������ ��������
			ASSERT_EQ(n, -1); // ��������� ������������ ��������
			for (int i=1; i<=j; i++) // ���� �� ������ �� ������ �����
			{
				ASSERT_EQ(begin->key, i); // ��������� �������� (������� �� ������ ���� ����������)
				begin = begin->next;
			}
			for (int i=k; i>j; i--) // ���� �� �����
			{
				ASSERT_EQ(begin->key, i);
				begin = begin->next;
			}
		}
	}
}
TEST(ftest, fline) // ���� ��������������� ������� f (��� �����)
{
	List* begin;
	begin = f(0); // ������� ������� ������
	ASSERT_EQ(bool(begin), 0); //  ��������� ������� ������
	for (int k=1; k<=10; k++) // ������ ������
	{
		begin = f(k); // ������� ������
		for (int i =1; i<=k; i++)
		{
			ASSERT_EQ(i, begin->key);
			begin = begin->next; // ������� ������������ ���������� ������
		}
		ASSERT_EQ(bool(begin), 0); // ���������, ���� ��������� ��������� �������
	}
}
TEST(ftest, fcycle) // ���� ��������������� ������� f (� ������)
{
	List* begin;
	List* t;
	for (int k=1; k<=10; k++)  // ������ ������
	{
		for (int j=1; j<=k; j++) // ���� ��������� end->next;
		{
			begin = f(k, j); // ������� ������ ����� k, � ������� ��������� ������� ��������� �� j-��
			t = begin; // ��������������� ���������
			for (int i=1; i<j; i++)
				t = t->next; // ������� �� j ��������
			for (int i=1; i<=k; i++)
			{
				ASSERT_EQ(begin->key, i);
				begin = begin->next;  // ��������� ������������ ���������� ������
			}
			ASSERT_EQ(t->key, begin->key); // ���������, ���� ��������� ��������� �������
		}
	}
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}