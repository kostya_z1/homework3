#include "Syntax.h"

bool Strcmp (const char a[], const char b [])
{
	int i = 0;
	while (a[i]!=0 && b[i]!=0)
	{
		if (a[i]!=b[i]) return 0;
		i++;
	}
	if (a[i]!=0 || b[i]!=0) return 0;
	return 1;
}

void Strcpy (char a[], const char b[])
{
	int i = 0;
	while (b[i]!=0)
	{
		a[i]=b[i];
		i++;
	}
	a[i]=b[i];
}

void Copy (int a[], const int b[])
{
	for (int i=0; i<5; i++)
		a[i] = b[i];
}

int Abs (int a)
{
	if (a<0) return -a;
	return a;
}

bool pr (const int a,const int b)
{
	if ((a==PLUS || a == MINUS) && b!=OPEN_STAPLE) return 1;
	if ((a==DEGR || a==MULT) && (b==DEGR || b==MULT)) return 1;
	return 0;
}

void need_init (char var[], Program &program)
{
	bool b = true;
	program.in.move_first();
	while (!program.in.is_close() && b)
	{
		if (Strcmp(program.in.get_value(), var))
			b = false;
		program.in.move_next();
	}
	program.commands.move_first();
	while (!program.commands.is_close())
	{
		if (Strcmp(program.commands.get_value().var_result, var))
			b = false;
		program.commands.move_next();
	}
	if (b)
		program.in.Push_back_char(var);
}

void syntax_analyzer(List<Lexeme> &lexemes, Program& program) 
{
	lexemes.move_first();
	stack<int> s;
	while (lexemes.get_const_value().type != POINT)
	{
		Node<Command> *c = new Node<Command>;
		Node<Lexeme> *temp = NULL;
		Strcpy(c->key.var_result, lexemes.get_const_value().var_name);
		lexemes.move_next();
		while (lexemes.get_const_value().type!=COLON)
		{
			lexemes.move_next();
			switch (lexemes.get_const_value().type)
			{
			case VAR: 
				need_init(lexemes.get_value().var_name, program);
				temp = new Node<Lexeme>;
				temp->key.type = VAR;
				Strcpy (temp->key.var_name, lexemes.get_const_value().var_name);
				c->key.rpn.Push_back(temp);
				break;
			case ARRAY:
				temp = new Node<Lexeme>;
				temp->key.type = ARRAY;
				Copy(temp->key.value, lexemes.get_const_value().value);
				c->key.rpn.Push_back(temp);
				break;
			case PLUS:
			case MINUS:
			case DEGR:
			case MULT:
				while (!s.empty()  && pr(lexemes.get_const_value().type, s.top()))
				{
					temp = new Node<Lexeme>;
					temp->key.type = s.top();
					c->key.rpn.Push_back(temp);
					s.pop();
				}
				s.push(lexemes.get_const_value().type);
				break;
			case OPEN_STAPLE:
				s.push(OPEN_STAPLE);
				break;
			case CLOSE_STAPLE:
				while (!s.empty() && s.top()!=OPEN_STAPLE)
				{
					temp = new Node<Lexeme>;
					temp->key.type = s.top();
					c->key.rpn.Push_back(temp);
					s.pop();
				}
				s.pop();
			}
		}
		lexemes.move_next();
		while (!s.empty())
		{
			temp = new Node<Lexeme>;
			temp->key.type = s.top();
			c->key.rpn.Push_back(temp);
			s.pop();
		}
		program.commands.Push_back(c);
	}


}