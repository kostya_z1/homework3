#ifndef _LIST_H_
#define _LIST_H_
#include <iostream>

template <class T>
struct Node
{
	T key;
	Node<T>* next;
};

template <class T> class List
{
private:

	Node<T>* begin;
	Node<T>* cur;
	Node<T>* end;
public:
	List() { begin = cur = end = 0; };
	~List();
	void Push_back(Node<T> *elem);
	void Push_back_char(char elem[]);
	void Push_back_int(int elem[]);
	void move_first ();
	void move_next ();
	bool is_close () const;
	bool is_empty () const;
	T& get_value () const;
	const T& get_const_value () const;
};

template <class T>
List<T>::~List()
{
	while (begin)
	{
		Node<T> *temp = begin->next;
		delete begin;
		begin = temp;
	}
}

template <class T>
void List<T>::Push_back(Node<T> *lex)
{
	lex->next = NULL;
	if (is_empty()) 
	{
		begin = end = cur = lex;
		return;
	}
	end->next = lex;
	end = lex;
}

template <class T>
void List<T>::move_first () 
{ 
	cur = begin;
}

template <class T>
void List<T>::move_next () 
{ 
	cur = cur->next;
}

template <class T>
bool List<T>::is_close () const
{ 
	if (cur==0) return 1;
	else return 0;
}

template <class T>
bool List<T>::is_empty () const
{
	if (begin==0) return 1;
	else return 0;
}

template <class T>
T& List<T>::get_value () const
{
	return cur->key;
}

template <class T>
void List<T>::Push_back_char(char elem[])
{
	Node<T> *lex = new Node<T>;
	int i=0;
	for (; elem[i]!=0; i++)
		lex->key[i] = elem[i];
	lex->key[i] = elem[i];
	lex->next = NULL;
	if (is_empty()) 
	{
		begin = end = cur = lex;
		return;
	}
	end->next = lex;
	end = lex;
}

template <class T>
const T& List<T>::get_const_value () const
{
	return cur->key;
}

template <class T>
void List<T>::Push_back_int(int elem[])
{
	Node<T> *lex = new Node<T>;
	int i=0;
	for (int i=0; i<5; i++)
		lex->key.value[i] = elem[i];
	lex->next = NULL;
	if (is_empty()) 
	{
		begin = end = cur = lex;
		return;
	}
	end->next = lex;
	end = lex;
}



#endif