#ifndef _LEXER_H_
#define _LEXER_H_
#include "types.h"
#include "List.h"
#include <iostream>
using namespace std;


const int END = 500;

bool is_char (char a);

bool is_num (char a);

void clear_char (char a[]);

void clear_int (int a[]);

bool is_znak (char a);

int znak (char a);

int Sum (int a[]);

void Copy (int a[], int b[]);

bool lex_analyzer(istream& in, List<Lexeme>& lexemes);

#endif