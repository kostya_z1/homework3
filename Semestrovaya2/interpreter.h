#ifndef _INTERPRETER_H_
#define _INTERPRETER_H_

#include "types.h"
#include "List.h"
#include "Syntax.h"
#include <iostream>



using namespace std;
void interpret(istream& in, Program &program, List<Var> &vars);
void Search(List<Lexeme> &lex, List<Var> & var);
void oper (stack<int> &s, int type);

#endif