#include "interpreter.h"

void Search(List<Lexeme> &lex, List<Var> & var)
{
	lex.move_first();
	while (!lex.is_close())
	{
		if (lex.get_value().type==VAR)
		{
			var.move_first();
			while (!var.is_close())
			{
				if (Strcmp(var.get_value().name, lex.get_value().var_name))
					Copy(lex.get_value().value, var.get_value().value);
				var.move_next();
			}
		}
		lex.move_next();
	}
}
void Search(List<Var> &var,const char s[], int t[])
{
	var.move_first();
	while (!var.is_close())
	{
		if (Strcmp(var.get_const_value().name, s))
		{
			for (int i=0; i<5; i++)
				var.get_value().value[i] = t[i];
			return;
		}
		var.move_next();
	}
	Node<Var> *temp = new Node<Var>;
	Strcpy(temp->key.name, s);
	for (int i=0; i<5; i++)
		temp->key.value[i] = t[i];
	var.Push_back(temp);
}


void oper (stack<int> &s, int type)
{
	int a[5], b[5];
	for (int i=4; i>=0; i--)
	{
		b[i] = s.top();
		s.pop();
	}
	for (int i=4; i>=0; i--)
	{
		a[i] = s.top();
		s.pop();
	}
	switch (type)
	{
	case PLUS:
		for (int i=0; i<5; i++)
			a[i]+=b[i];
		break;
	case MINUS:
		for (int i=0; i<5; i++)
			a[i]-=b[i];
		break;
	case DEGR:
		for (int i=0; i<5; i++)
			a[i]/=b[i];
		break;
	case MULT:
		for (int i=0; i<5; i++)
			a[i]*=b[i];
		break;
	}
	for (int i = 0; i<5; i++)
		s.push(a[i]);
}

void interpret(istream& in, Program &program, List<Var> &vars) 
{
	program.in.move_first();
	while (!program.in.is_close())
	{
		Node<Var> *temp = new Node<Var>;
		Strcpy(temp->key.name, program.in.get_const_value());
		cout << "input " << program.in.get_value() << " : ";
		for (int i=0; i<5; i++)
			in >> temp->key.value[i];
		cout << endl;
		vars.Push_back(temp);
		program.in.move_next();
	}
	program.commands.move_first();
	while (!program.commands.is_close())
	{
		Search(program.commands.get_value().rpn, vars);
		stack<int> s;
		program.commands.get_value().rpn.move_first();
		while (!program.commands.get_value().rpn.is_close())
		{
			int type = program.commands.get_value().rpn.get_value().type;
			if (type == VAR || type == ARRAY)
				for (int i=0; i<5; i++)
					s.push(program.commands.get_value().rpn.get_value().value[i]);
			else oper (s, type);
			program.commands.get_value().rpn.move_next();
		}
		int t[5]={0};
		for (int i=4; i>=0; i--)
		{
			t[i] = s.top();
			s.pop();
		}
		Search(vars,  program.commands.get_const_value().var_result, t);
		/*
		Node<Var> *t = new Node<Var>;
		Strcpy(t->key.name, program.commands.get_const_value().var_result);
		for (int i=4; i>=0; i--)
		{
			t->key.value[i] = s.top();
			s.pop();
		}
		vars.Push_back(t);
		*/
		program.commands.move_next();
	}

}