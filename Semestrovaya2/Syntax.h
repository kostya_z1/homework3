#ifndef _SYNTAX_H_
#define _SYNTAX_H_

#include "types.h"
#include "List.h"
#include <stack>
using std::stack;
void syntax_analyzer(List<Lexeme> &lexemes, Program& program);
void Copy (int a[], const int b[]);
void Strcpy (char a[], const char b[]);
bool Strcmp (const char a[], const char b []);
void need_init (char var[], Program &program);
bool pr (const int a,const int b);
int Abs (int a);

#endif