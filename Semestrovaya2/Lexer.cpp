#include "Lexer.h"

// �������� �� "a" ������
bool is_char (char a) 
{
	if ((a>='a' && a<='z') || (a>='A' && a<='Z')) return 1;
	return 0;
}	 

// �������� �� "a" ������
bool is_num (char a)
{
	if (a>='0' && a<='9') return 1;
	return 0;
}

// ��������� ���������� �������, ��������� ��� ����������
void clear_char (char a[])
{
	for (int i=0; i<M_VAR_SIZE+1; i++)
		a[i] = 0;
}

// ������ ���������� �������, ��������� �������� 
void clear_int (int a[])
{
	for (int i=0; i<ARRAY_SIZE; i++)
		a[i] = 0;
}

// �������� �� "a" ������
bool is_znak (char a)
{
	if (a=='+' || a=='-' || a=='*' || a=='/') return 1;
	return 0;
}

// ���������� ��� �����
int znak (char a)
{
	switch (a)
	{
		case '+': return PLUS;
		case '-': return  MINUS;
		case '*': return MULT;
		case '/': return DEGR;
	}
	return -1;
}

// ����������� ��������� ������ � �����
int Sum (int a[], int n)
{
	int s = 0, v = 1;
	for (int i = n-1; i>=0; i--)
	{
		s+=a[i]*v;
		v*=10;
	}
	return s;
}

// �������� ��������� ������ "b" � ��������� ������ "a"
void Copy (int a[], int b[])
{
	for (int i=0; i<ARRAY_SIZE; i++)
		a[i]=b[i];
}

// �������� ��������� ������ "b" � ��������� ������ "a"
void Copy (char a[], char b[])
{
	for (int i=0; i<M_VAR_SIZE+1; i++)
		a[i]=b[i];
}

bool lex_analyzer(istream& in, List<Lexeme>& lexemes)
{
	Node<Lexeme> * lex;
	int s = 0, // ������� ���������
		max_var = 0, // ������� ������� ����� ����������
		num_value[ARRAY_SIZE+1]={0},	// �������� � ������� �������
		r_max = 0, // ���-�� ������������� ������� � �������
		sum = 1, // ���� ����� + ��� -1
		max_num = 0, // ���-�� ���� � �����
		temp_num[ARRAY_SIZE+1] = {0}, 
		open_stap = 0, // ���-�� ������������� ������
		close_stap = 0; // ���-�� ������������� ������
	char a = 0, 
		var_name[M_VAR_SIZE+1]={0},
		a1; // ��� ������� ����������
	while (s!=-1 && s!=END)
	{
		a1 = a;
		in.get(a);
		switch (s)
		{
		case 0:  // ��������� 0: ������� ����� ��� ����� ��������� - �����
			if (a==' ') continue;
			if (a=='.') 
			{
				lex = new Node<Lexeme>;
				lex->key.type = POINT;
				lexemes.Push_back(lex);
				s = END;
				break;
			}
			else if (is_char(a)) // ���� �����, �� �� ����� ��� ����������
			{
				s = 1;
				var_name[max_var] = a;
				max_var++;
			}
			else s = -1;
			break;
		case 1:	// ��������� 1:  ������� ��������� ��� ����������� ����� ���������� - ����� ��� �����
			if (a==' ') continue;
			if (a==':' ) 
			{
				lex = new Node<Lexeme>;
				Copy(lex->key.var_name, var_name);
				lex->key.type = VAR;
				lexemes.Push_back(lex); // ��������� - ���������� �����������, ��������� �� � ������
				max_var = 0; // �������� ������� ����� ����������
				clear_char(var_name); // ������ ��� ����������
				s = 2;
			}
			else if ((is_char(a) || is_num(a)) && a1!=' ' ) // ����� ��� �����, ������ �� ��� ��� ������ ��� ����������
			{
				var_name[max_var] = a;
				max_var++;
			}
			else s = -1;
			if (max_var>M_VAR_SIZE) // ������ ���������� ������ ��������� �����? ������
				s = -1;
			break;
		case 2:	// ��������� 2: ������� ���� �����
			if (a!='=') s = -1; // �� ���� �����? ������
			if (a=='=') // ���� �����, ������ � ��� ���� ������������, ������� ��� � ������
			{
				lex = new Node<Lexeme>;
				lex->key.type = ASSIGN;
				lexemes.Push_back(lex);
				s = 3;
			}
			break;
		case 3:	// ��������� 3: ������� '(' , ������ ������� '{' ��� ������ ���������� - �����
			if (a==' ') continue;
			if (a=='{') // ���� '{', ������ ����� ���� ������
				s = 5;
			else if (is_char(a)) // ���� �����, �� ����� ���� ����������
			{
				var_name[max_var] = a;
				max_var++;
				s = 4;
			}
			else if (a=='(') // ���� ����� ���� '(', �� ������ ������ �� ���� (����� ���������� �� � ������)
			{
				lex = new Node<Lexeme>;
				lex->key.type = OPEN_STAPLE;
				lexemes.Push_back(lex);
				open_stap++; // �������������� ���-�� ���� ������
			}
			else s = -1;
			break;
		case 4: // ��������� 4: ��������� ';', ������, �������� ��� �������� ������
			if (a==' ') continue;
			if ((is_char(a) || is_num(a))&& a1!=' ')
			{
				var_name[max_var] = a; // ���� ������, ���������� ���������� ��� ����������
				max_var++;
			}
			else if (a==';' && close_stap == open_stap) // ���� ��������� � �� �������� �� ���������� 
			{
				lex = new Node<Lexeme>; 
				Copy(lex->key.var_name, var_name);
				lex->key.type = VAR;
				lexemes.Push_back(lex); // ��������� ������� ����������, ��� ������� ��������
				lex = new Node<Lexeme>;
				lex->key.type = COLON;
				lexemes.Push_back(lex); // ��������� ��������������� ���������
				max_var = 0; // �������� ������ ����� ����������
				clear_char(var_name); // ������ ���
				close_stap = open_stap = 0; // �������� ��������� ������
				s = 0; // � ������
			}
			else if (is_znak(a) )  // ���� �������� - ������ ����� ����, ��� � ����
			{
				lex = new Node<Lexeme>;
				Copy(lex->key.var_name, var_name);
				lex->key.type = VAR;
				lexemes.Push_back(lex);
				max_var = 0;
				clear_char(var_name);
				lex = new Node<Lexeme>;
				lex->key.type = znak(a);
				lexemes.Push_back(lex);
				s = 3;
			}
			else if (a==')')
			{
				lex = new Node<Lexeme>;
				Copy(lex->key.var_name, var_name);
				lex->key.type = VAR;
				lexemes.Push_back(lex);
				lex = new Node<Lexeme>;
				lex->key.type = CLOSE_STAPLE;
				lexemes.Push_back(lex);
				clear_char(var_name);
				max_var = 0;
				close_stap++;
				if (close_stap>open_stap) s = -1;
				s = 8;
			}
			else s = -1;
			if (max_var>M_VAR_SIZE) 
				s = -1;
			break;
		case 5: // ��������� 5: ������� ����� - ����� i-��� ����� � �������
			if (a==' ') continue;
			if (a=='+')
				s = 6;				// ��������� ����
			else if (a=='-')
			{
				sum = -1;
				s = 6;
			}	
			else s = -1;
			break;
		case 6:	//	��������� 6: ������� �����
			if (is_num(a))	
			{
				temp_num[max_num] = a-'0';
				max_num++;
				s = 7;
			}
			else s = -1;
			break;
		case 7: // ��������� 7: ������� ����� ��� ',' ��� '}'
			if (a==' ') continue;
			if (a==',') // ����������� ������� 
			{
				num_value[r_max] = sum*Sum(temp_num, max_num);	// ����������� ��������, ������� �������� � ������� ��� ���� ����
				clear_int(temp_num);
				max_num = 0;
				sum = 1;	// ��� ������ � ��������
				r_max++;
				s = 5;
			}
			else if (is_num(a) && a1!= ' ')
			{
				temp_num[max_num] = a-'0';
				max_num++;
			}
			else if (a=='}' && r_max==4) // ���� ������ ��������� ������
			{
				lex = new Node<Lexeme>;
				num_value[4] = sum*Sum(temp_num, max_num);
				Copy (lex->key.value, num_value);
				lex->key.type = ARRAY;
				lexemes.Push_back(lex);
				clear_int(temp_num);
				sum = 1;
				r_max = 0;
				max_num = 0;
				s = 8; // ��������� ��� � ��� ������
			}
			else s = -1;
			if (max_num>5) s = -1;
			break;
		case 8:
			if (a==' ' ) continue;
			if (a==';') 
			{
				lex = new Node<Lexeme>;
				lex->key.type = COLON;
				lexemes.Push_back(lex);
				close_stap = open_stap = 0;
				s = 0;
			}
			else if (a==')')
			{
				lex = new Node<Lexeme>;
				lex->key.type = CLOSE_STAPLE;
				lexemes.Push_back(lex);
				close_stap++;
				if (close_stap>open_stap) s = -1;
			}
			else if (is_znak(a))
			{
				lex = new Node<Lexeme>;
				lex->key.type = znak(a);
				lexemes.Push_back(lex);
				s = 3;
			}
			else s = -1;
			break;
		}
	}
	if (s==END) return 1;
	else return 0;
}