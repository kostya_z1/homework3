#ifndef _TYPES_H_
#define _TYPES_H_
#include "List.h"
#include <string.h>
const int OPEN_STAPLE = 0; // ���� ������
const int ARRAY = 1; // ������
const int VAR = 2; // ����������
const int PLUS = 3; // ����
const int MINUS = 4; // �����
const int MULT = 5; // ���������
const int DEGR = 6; // �������
const int COLON = 7; // ����� � �������
const int POINT = 8; // �����
const int ASSIGN = 9; // ������������
const int CLOSE_STAPLE = 10; // ������������� ������
const int ARRAY_SIZE = 5; // ������ �������
const int M_VAR_SIZE = 4; // ������������ ����� ����������

struct Lexeme {
  int type;
  char var_name[10];
  int value[5];
};
 

struct Command {
  char var_result[100];
  List<Lexeme> rpn;
};

struct Program {
  List<char[10]> in;
  List<Command> commands;
};

struct Var {
  char name[10];
  int value[5];
};

#endif